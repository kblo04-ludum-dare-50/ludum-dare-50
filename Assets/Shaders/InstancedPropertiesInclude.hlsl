UNITY_INSTANCING_BUFFER_START(Props)
UNITY_DEFINE_INSTANCED_PROP(float4, _InstanceColor)
UNITY_DEFINE_INSTANCED_PROP(float, _Jump)
UNITY_DEFINE_INSTANCED_PROP(float, _JumpOffset)
UNITY_DEFINE_INSTANCED_PROP(float, _JumpSpeed)
UNITY_INSTANCING_BUFFER_END(Props)
 
void InstanceProperties_float(out float4 InstanceColor, out float Jump, out float JumpOffset, out float JumpSpeed)
{
	InstanceColor = UNITY_ACCESS_INSTANCED_PROP(Props, _InstanceColor);
	Jump = UNITY_ACCESS_INSTANCED_PROP(Props, _Jump);
	JumpOffset = UNITY_ACCESS_INSTANCED_PROP(Props, _JumpOffset);
	JumpSpeed = UNITY_ACCESS_INSTANCED_PROP(Props, _JumpSpeed);
}