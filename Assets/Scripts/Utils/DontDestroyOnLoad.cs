using System;
using System.Linq;
using UnityEngine;

namespace KB.Utils
{
    public class DontDestroyOnLoad : MonoBehaviour
    {
        protected virtual void Awake() => DontDestroyOnLoad(gameObject);

        protected static void CreateSingleton(Type type, GameObject prefab = null, string name = "")
        {
            if (FindObjectOfType(type))
                return;

            if (string.IsNullOrEmpty(name))
                name = SingletonName(type.Name);

            if(prefab == null)
                new GameObject(name, type);
            else
                Instantiate(prefab).name = name;
        }

        private static string SingletonName(string originalName)
        {
            char[] upperCases = originalName.Where(char.IsUpper).ToArray();
            string result = originalName;

            for (int i = 1; i < upperCases.Length; i++)
                result = result.Insert(originalName.IndexOf(upperCases[i]), " ");

            return string.Format("[{0}]", result);
        }
    }
}