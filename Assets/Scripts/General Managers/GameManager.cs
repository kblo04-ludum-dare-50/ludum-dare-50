#if UNITY_EDITOR
using UnityEditor;
#endif

using UnityEngine;
using UnityEngine.Events;

#region Enums

public enum GameState
{
    Menu,
    Gameplay,
    Result
}

#endregion

public static class GameManager
{
    #region Actions

    public static event UnityAction<GameState> OnStateChanged;
    public static event UnityAction OnPause;
    public static event UnityAction OnResume;
    public static event UnityAction OnQuit;

    #endregion

    #region Variables

    private static GameState _state;
    private static bool _isPaused;

    #endregion

    #region Properties

    public static GameState State => _state;
    public static bool IsPaused => _isPaused;

    #endregion

    #region Methods

    public static void SetState(GameState state)
    {
        _state = state;
        OnStateChanged?.Invoke(_state);

        if (_state == GameState.Gameplay)
            return;

        Resume();
    }

    public static void Pause()
    {
        Time.timeScale = 0f;
        _isPaused = true;
        OnPause?.Invoke();
    }

    public static void Resume()
    {
        Time.timeScale = 1f;
        _isPaused = false;
        OnResume?.Invoke();
    }

    public static void Quit()
    {
#if UNITY_EDITOR
        EditorApplication.ExitPlaymode();
#else
        Application.Quit();   
#endif

        OnQuit?.Invoke();
    }

    #endregion
}
