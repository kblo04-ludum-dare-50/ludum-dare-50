using KB.Utils;
using UnityEngine;
using UnityEngine.Events;

public class ScoreManager : DontDestroyOnLoad
{
    #region Variables

    private static int _currentScore;

    private static event UnityAction<int> OnScoreChanged;

    #endregion

    #region Properties

    public static int CurrentScore => _currentScore;

    #endregion

    #region Initialization

    [RuntimeInitializeOnLoadMethod]
    private static void Init() => CreateSingleton(typeof(ScoreManager));

    #endregion

    #region Methods

    public static void AddScore(int value)
    {
        _currentScore += value;
        OnScoreChanged?.Invoke(value);
    }

    public static void SaveScore()
    {
        //TODO: Adicionar current score � lista do leaderboard no final da partida
    }

    public static void ResetScore()
    {
        _currentScore = 0;
    }

    #endregion
}
