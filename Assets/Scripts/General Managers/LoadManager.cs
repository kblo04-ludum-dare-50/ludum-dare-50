using System.Collections;
using UnityEngine;
using KB.Utils;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class LoadManager : DontDestroyOnLoad
{
    #region Variables

    private static event UnityAction<int, LoadSceneMode> OnLoadScene;
    private static event UnityAction<int, UnloadSceneOptions> OnUnloadScene;
    private static bool _isLoading;
    private static bool _isUnloading;
    private static float _loadProgress;

    #endregion

    #region Properties

    public static bool IsLoading => _isLoading;
    public static bool IsUnloading => _isUnloading;
    internal static float LoadProgress => _loadProgress;

    #endregion

    #region Initialization

    [RuntimeInitializeOnLoadMethod]
    private static void Init() => CreateSingleton(typeof(LoadManager), name: "[Load Manager]");

    #endregion

    #region Messages

    private void OnEnable()
    {
        OnLoadScene += StartLoadScene;
        OnUnloadScene += StartUnloadScene;
    }

    private void OnDisable()
    {
        OnLoadScene -= StartLoadScene;
        OnUnloadScene -= StartUnloadScene;
    }

    #endregion

    #region Methods

    #region Load

    internal static void LoadScene(int buildIndex, LoadSceneMode mode)
    {
        if (_isLoading)
        {
            Debug.LogError("Another scene is already being loaded.");
            return;
        }

        OnLoadScene.Invoke(buildIndex, mode);
    }

    private void StartLoadScene(int buildIndex, LoadSceneMode mode)
    {
        StartCoroutine(LoadSceneAsync(buildIndex, mode));
    }

    private IEnumerator LoadSceneAsync(int buildIndex, LoadSceneMode mode)
    {
        _isLoading = true;
        _loadProgress = 0;
        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(buildIndex, mode);

        while (!asyncOperation.isDone)
        {
            _loadProgress = Mathf.Clamp01(asyncOperation.progress / 0.9f);
            yield return null;
        }

        _loadProgress = 1;
        _isLoading = false;
    }

    #endregion

    #region Unload

    internal static void UnloadScene(int buildIndex, UnloadSceneOptions options)
    {
        OnUnloadScene.Invoke(buildIndex, options);
    }

    private void StartUnloadScene(int buildIndex, UnloadSceneOptions options)
    {
        StartCoroutine(UnloadSceneAsync(buildIndex, options));
    }

    private IEnumerator UnloadSceneAsync(int buildIndex, UnloadSceneOptions options)
    {
        _isUnloading = true;
        AsyncOperation asyncOperation = SceneManager.UnloadSceneAsync(buildIndex, options);

        yield return new WaitUntil(() => asyncOperation.isDone);
        _isUnloading = false;
    }

    #endregion

    #endregion
}
