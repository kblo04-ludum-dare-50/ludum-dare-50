using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using KB.Utils;
using KB.PoolSystem;
using UnityEngine.Events;
using System;
using System.Linq;

public class DynamicObjManager : DontDestroyOnLoad
{
    #region Constants

    private const string DataPath = "Dynamic Object Manager Data";

    #endregion

    #region Variables

    public static DynamicObjManagerData data;
    private static Pool<Body> bodyPool = new Pool<Body>();
    private static Pool<Building> buildingPool = new Pool<Building>();

    private static Transform bodyContainer;
    private static Transform buildingContainer;

    //public static List<Transform> activeBodies = new List<Transform>();

    //public static int jumpOffsetID = Shader.PropertyToID("_JumpOffset");
    #endregion

    #region Initialization

    [RuntimeInitializeOnLoadMethod]
    private static void Init() => CreateSingleton(typeof(DynamicObjManager), name: "[Dynamic Object Manager]");

    #endregion

    #region Messages

    protected override void Awake()
    {
        base.Awake();
        SetContainers();
        data = Resources.Load<DynamicObjManagerData>(DataPath);
        SetPools();
    }

    private void OnEnable()
    {
    }

    private void OnDisable()
    {
    }

    /*private void Update()
    {
        if(activeBodies.Count <= 0)
            return;

        InstancedRenderer.UpdatePositions(data.instancedMeshData, activeBodies.ToArray());
    }*/

    #endregion

    #region Methods
    private void SetContainers()
    {
        bodyContainer = new GameObject("Body Container").transform;
        bodyContainer.SetParent(transform, false);

        buildingContainer = new GameObject("Building Container").transform;
        buildingContainer.SetParent(transform, false);
    }
    private void SetPools()
    {
        bodyPool.SetPool(data.bodyStartCount, data.bodyPrefab, bodyContainer);
        buildingPool.SetPool(data.buildingStartCount, data.buildingPrefab, buildingContainer);
    }

    #region Instantiate

    public static void InstantiateBody(Vector3 position, Region region, Quaternion rotation)
    {
        Body newBody = bodyPool.GetObject();
        SetPosition(newBody, position, rotation, region);
        newBody.InitializeBody(region, bodyPool, bodyContainer);
        //activeBodies.Add(newBody.transform);
    }

    public static void InstantiateBuilding(Vector3 position, Region region, Quaternion rotation)
    {
        Building newBuilding = buildingPool.GetObject();
        SetPosition(newBuilding, position, rotation, region);

        newBuilding.InitializeBuilding(buildingPool, buildingContainer);
    }

    private static void SetPosition(DynamicObj dynamicObj, Vector3 position, Quaternion rotation, Region region)
    {
        dynamicObj.transform.position = position;
        dynamicObj.transform.rotation = rotation;
        //dynamicObj.transform.SetParent(region.transform, true);
    }

    #endregion

    #endregion
}
