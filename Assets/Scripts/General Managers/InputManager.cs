using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using KB.Utils;

public class InputManager : DontDestroyOnLoad
{
    #region Variables

    private PlayerInput _playerInput;

    public static class Maps
    {
        public const string UI = "UI";
        public const string Player = "Player";
    }

    public static UnityEvent<string> ChangeMap = new UnityEvent<string>();



    #region Axis

    public static Vector2 Move { get; private set; }
    public static Vector2 Look { get; private set; }
    public static Vector2 Zoom { get; private set; }

    #endregion

    #region Button Actions

    public static UnityAction Fire;

    #endregion

    #endregion

    [RuntimeInitializeOnLoadMethod]
    private static void Init() => CreateSingleton(typeof(InputManager), Resources.Load<GameObject>("Managers/[Input Manager]"), "[Input Manager]");

    #region Messages

    protected override void Awake()
    {
        base.Awake();
        _playerInput = GetComponent<PlayerInput>();
    }

    private void OnEnable()
    {
        ChangeMap.AddListener(_playerInput.SwitchCurrentActionMap);
        

    }

    private void OnDisable()
    {
        ChangeMap.RemoveListener(_playerInput.SwitchCurrentActionMap);
    }

    #endregion

    #region Input Messages

    public void OnMove(InputValue inputValue)
    {
        Move = inputValue.Get<Vector2>();
        if (!UITutorial.hasRotate && GameManager.State == GameState.Gameplay)
            UITutorial.hasRotate = true;
    }
    public void OnLook(InputValue inputValue) => Look = inputValue.Get<Vector2>();
    public void OnZoom(InputValue inputValue) => Zoom = inputValue.Get<Vector2>();
    public void OnFire()
    {
        Fire?.Invoke();
        if (!UITutorial.hasShoot && GameManager.State == GameState.Gameplay)
            UITutorial.hasShoot = true;
    }
    public void OnPause() => GameManager.Pause();
    public void OnResume() => GameManager.Resume();

    #endregion

    #region Private Methods

    #endregion

}
