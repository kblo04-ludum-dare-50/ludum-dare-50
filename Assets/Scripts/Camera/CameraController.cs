using Cinemachine;
using UnityEngine;

[RequireComponent(typeof(CinemachineVirtualCamera))]
public class CameraController : MonoBehaviour
{
    #region Variables

    [SerializeField, Range(1, 10)] private float _sensibility;
    [SerializeField, Range(0, 1)] private float _smoothing;
    [SerializeField] private Vector4 _zoomMin;
    [SerializeField] private Vector4 _zoomMax;

    private CinemachineVirtualCamera _virtualCamera;
    private CinemachineOrbitalTransposer _orbitalTransposer;
    private CinemachineComposer _composer;
    private float _lerp;
    private Vector3 _offsetVelocity;
    private float _zoomVelocity;

    #endregion

    #region Messages

    private void Awake()
    {
        _virtualCamera = GetComponent<CinemachineVirtualCamera>();
        _orbitalTransposer = _virtualCamera.GetCinemachineComponent<CinemachineOrbitalTransposer>();
        _composer = _virtualCamera.GetCinemachineComponent<CinemachineComposer>();
    }

    private void OnEnable()
    {
        _lerp = 0.5f;
        _orbitalTransposer.m_FollowOffset = Vector3.Lerp(_zoomMin, _zoomMax, _lerp);
        _composer.m_TrackedObjectOffset = Mathf.Lerp(_zoomMin.w, _zoomMax.w, _lerp) * Vector3.up;
    }

    private void Update()
    {
        _lerp += InputManager.Zoom.y * Time.deltaTime * _sensibility;
        _lerp = Mathf.Clamp01(_lerp);

        Vector3 offset = Vector3.SmoothDamp(_orbitalTransposer.m_FollowOffset, Vector3.Lerp(_zoomMin, _zoomMax, _lerp), ref _offsetVelocity, _smoothing);
        float zoom = Mathf.SmoothDamp(_composer.m_TrackedObjectOffset.y, Mathf.Lerp(_zoomMin.w, _zoomMax.w, _lerp), ref _zoomVelocity, _smoothing);

        _orbitalTransposer.m_FollowOffset = offset;
        _composer.m_TrackedObjectOffset = zoom * Vector3.up;
    }

    #endregion
}
