using Cinemachine;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(CinemachineImpulseSource))]
public class CameraShake : MonoBehaviour
{
    private CinemachineImpulseSource _impulseSource;
    private static event UnityAction OnShake;

    private void Awake()
    {
        _impulseSource = GetComponent<CinemachineImpulseSource>();
    }

    private void OnEnable()
    {
        OnShake += Impulse;
    }

    private void OnDisable()
    {
        OnShake += Impulse;
    }

    public static void Shake() => OnShake?.Invoke();


    private void Impulse()
    {
        _impulseSource.GenerateImpulse(transform.position);
    }
}
