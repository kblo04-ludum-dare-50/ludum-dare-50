using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Objects/VFX System/VFX Data")]
public class VFXData : ScriptableObject
{
    [Header("Data")]
    public GameObject prefab;

    [Header("General Settings")]
    [Min(0)] public float duration;
    [Min(0)] public int poolSize;

    [Header("Spawn Settings")]
    public bool setParent;
}
