using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Objects/VFX System/VFX Manager Data")]
public class VFXManagerData : ScriptableObject
{
    public VFXData[] vfxDatas;
}
