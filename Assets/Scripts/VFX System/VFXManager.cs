using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using KB.Utils;
using KB.PoolSystem;
using UnityEngine.Events;
using System;

public class VFXManager : DontDestroyOnLoad
{
    #region Constants

    private const string DataPath = "VFX Manager Data";

    #endregion

    #region Variables

    private static VFXManagerData data;
    private static Dictionary<VFXData, Pool<ParticleSystem>> pools = new Dictionary<VFXData, Pool<ParticleSystem>>();

    private static UnityEvent<VFXData, ParticleSystem> OnRequestRecycle = new UnityEvent<VFXData, ParticleSystem>();

    #endregion

    #region Iniialization

    [RuntimeInitializeOnLoadMethod]
    private static void Init() => CreateSingleton(typeof(VFXManager), name: "[VFX Manager]");

    #endregion

    #region Messages

    protected override void Awake()
    {
        base.Awake();

        data = Resources.Load<VFXManagerData>(DataPath);
        SetPools();
    }

    private void OnEnable()
    {
        OnRequestRecycle.AddListener(RequestRecycle);
    }

    private void OnDisable()
    {
        OnRequestRecycle.RemoveListener(RequestRecycle);
    }

    #endregion

    #region Methods

    private void SetPools()
    {
        pools.Clear();

        foreach (VFXData data in data.vfxDatas)
        {
            Transform parent = new GameObject(data.name).transform;
            parent.SetParent(transform);
            var pool = new Pool<ParticleSystem>();
            pool.SetPool(data.poolSize, data.prefab, parent);

            pools.Add(data, pool);
        }
    }

    #region Play

    public static void PlayVFX(VFXData vfxData, Transform transform = null, Vector3? offset = null)
    {
        if (!pools.TryGetValue(vfxData, out var pool))
            return;

        ParticleSystem particleSystem = pool.GetObject();
        SetPosition(particleSystem.transform, transform, offset, vfxData.setParent);

        particleSystem.Play();
        OnRequestRecycle.Invoke(vfxData, particleSystem);
    }

    private static void SetPosition(Transform particleSystem, Transform transform, Vector3? offset, bool setParent)
    {
        if (transform == null)
            return;

        Vector3 offsetPosition = offset.HasValue ? transform.TransformDirection(offset.Value) : Vector3.zero;

        if (setParent)
        {
            particleSystem.SetParent(transform);
            particleSystem.localPosition = offsetPosition;
        }

        else
        {
            particleSystem.position = transform.position + offsetPosition;
        }
    }

    #endregion

    #region Recycle

    private void RequestRecycle(VFXData vFXData, ParticleSystem particleSystem) => StartCoroutine(Recycle(vFXData, particleSystem));

    private IEnumerator Recycle(VFXData data, ParticleSystem particleSystem)
    {
        yield return new WaitForSeconds(data.duration);

        Pool<ParticleSystem> pool = pools[data];

        if (particleSystem.transform.parent != pool.Parent)
        {
            particleSystem.transform.parent = pool.Parent;
            particleSystem.transform.localPosition = Vector3.zero;
        }

        particleSystem.Stop();
        pool.RecycleObject(particleSystem);
    }

    #endregion

    #endregion

    #region Structs

    [Serializable]
    public struct SpawnInfo
    {
        public VFXData data;
        public Vector3 offset;
    }

    #endregion
}
