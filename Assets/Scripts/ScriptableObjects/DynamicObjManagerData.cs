using System.Collections;
using System.Collections.Generic;

using UnityEngine;

[CreateAssetMenu(fileName = "DynamicObjManagerData", menuName = "Datas/DynamicObjManagerData", order = 0)]
public class DynamicObjManagerData : ScriptableObject
{
    [Header("Body Pool")]
    public int bodyStartCount;
    public GameObject bodyPrefab;

    [Header("Building Pool")]
    public int buildingStartCount;
    public GameObject buildingPrefab;

    public InstancedMeshData instancedMeshData;
}
