using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "RegionSpawnData", menuName = "Datas/RegionSpawnData", order = 0)]
public class RegionSpawnData : ScriptableObject
{
    public AnimationCurve spawnRate;
}
