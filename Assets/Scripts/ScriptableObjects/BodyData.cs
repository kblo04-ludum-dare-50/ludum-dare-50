using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BodyData", menuName = "Datas/BodyData", order = 0)]
public class BodyData : ScriptableObject
{
    public Vector2 moveSpeedRange;
    public Vector2 repositionTimeRange;
    public FXData FXData;
    public PhysicsData physicsData;
}
