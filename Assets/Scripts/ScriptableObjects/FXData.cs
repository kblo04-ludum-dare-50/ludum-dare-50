using UnityEngine;

[CreateAssetMenu(fileName = "FXData", menuName = "Datas/New FX Data")]
public class FXData : ScriptableObject
{
    public VFXData spawnVfxData;
    public VFXData destroyVfxData;
    public AudioData spawnAudioData;
    public AudioData destroyAudioData;
}
