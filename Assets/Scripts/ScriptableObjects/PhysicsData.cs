using UnityEngine;

[CreateAssetMenu(fileName = "PhysicsData", menuName = "Datas/New Physics Data")]
public class PhysicsData : ScriptableObject
{
    public float weight;
    public float drag;
    public float angularDrag;
}
