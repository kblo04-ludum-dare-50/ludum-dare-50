using System.Collections;
using System.Collections.Generic;

using UnityEngine;

[CreateAssetMenu(fileName = "GameData", menuName = "Datas/GameData", order = 0)]
public class GameData : ScriptableObject {
    public float maxGameTime;
    public float maxWorldInclination;
}
