using UnityEngine;

[CreateAssetMenu(fileName = "SpecificBuildingData", menuName = "Datas/New Specific Building Data")]
public class SpecificBuildingData : ScriptableObject
{
    public string buildingName;
    public float growthRate;
    public float growthPerPeople;
    public FXData FXData;
    public PhysicsData physicsData;
}
