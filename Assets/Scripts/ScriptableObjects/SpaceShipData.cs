using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SpaceShipData", menuName = "Datas/SpaceShipData", order = 0)]
public class SpaceShipData : ScriptableObject
{
    public float shipHeight = 10;
    public float distanceFromTargetToShot = 0.05f;
    public float shipSpeed = 1;

    [Header("Rotation")]
    public float lookUpSpeed = 10;
    public float lookDownSpeed = 10;
    public float lookDownHeight = 50;
    public float rotationTargetVirtualDistance = 50;
    
    [Space]
    public float explosionRadius = 10;
    public float shotDuration = 0;
    public float waitDuration = 0;
    public LayerMask laserLayerMask;
    public LayerMask groundLayerMask;

    [Header("VFX")]
    public VFXData shotVfxData;
    public Vector3 shotVfxOffSet;

    [Header("Shot SFX")]
    public AudioData shotAudioData;
    public AudioData shotImpactAudioData;
}
