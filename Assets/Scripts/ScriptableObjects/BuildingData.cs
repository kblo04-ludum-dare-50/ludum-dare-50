using UnityEngine;

[CreateAssetMenu(fileName = "BuildingData", menuName = "Datas/New Building Data")]
public class BuildingData : ScriptableObject
{
    public SpecificBuildingData[] specificBuildingData;
}
