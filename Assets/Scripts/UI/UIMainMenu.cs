using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIMainMenu : MonoBehaviour
{
    #region Variables

    [SerializeField] private Canvas _canvas;
    [SerializeField] private Button _start;
    [SerializeField] private Button _quit;

    #endregion

    #region Messages

    private void OnEnable()
    {
        _start.onClick.AddListener(Play);
        _quit.onClick.AddListener(Quit);
        GameManager.OnStateChanged += StateChanged;
    }

    private void OnDisable()
    {
        _start.onClick.RemoveListener(Play);
        _quit.onClick.RemoveListener(Quit);
        GameManager.OnStateChanged -= StateChanged;
    }

    #endregion

    #region Methods

    private void Play()
    {
        LoadManager.LoadScene(1, LoadSceneMode.Additive);
        StartCoroutine(SetGameplayState());
    }

    private IEnumerator SetGameplayState()
    {
        yield return null;
        yield return new WaitWhile(()=> LoadManager.IsLoading);

        GameManager.SetState(GameState.Gameplay);
    }

    private void Quit() => GameManager.Quit();

    private void StateChanged(GameState state)
    {
        _canvas.enabled = state == GameState.Menu;
    }

    #endregion
}
