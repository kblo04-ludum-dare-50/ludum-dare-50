using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using UnityEngine.Events;
public class UIOptions : MonoBehaviour
{

    #region Variables

    [SerializeField] private AudioMixerGroup _masterGroup;
    [SerializeField] private AudioMixerGroup _musicGroup;
    [SerializeField] private AudioMixerGroup _sfxGroup;

    [SerializeField] private Slider _masterSlider;
    [SerializeField] private Slider _musicSlider;
    [SerializeField] private Slider _sfxSlider;
    [SerializeField] private Toggle _toggleQuality;

    public static UnityAction UpdateValues;
    #endregion

    #region Messages

    void Start()
    {
        UpdateAudioMixers();
        if (QualitySettings.GetQualityLevel() == 0)
            _toggleQuality.isOn = false;
        else
            _toggleQuality.isOn = true;
    }

    private void OnEnable()
    {
        _toggleQuality.onValueChanged.AddListener(delegate { ToggleValueChanged(_toggleQuality); });
        UpdateValues += UpdateAudioMixers;
    }


    private void OnDisable()
    {
        _toggleQuality.onValueChanged.RemoveListener(delegate { ToggleValueChanged(_toggleQuality); });
        UpdateValues -= UpdateAudioMixers;
    }

    #endregion

    #region Public Methods

    public void UpdateMasterMixer(float sliderValue)
    {
        _masterGroup.audioMixer.SetFloat("volumeMaster", Mathf.Log10(sliderValue) * 20);
    }

    public void UpdateMusicMixer(float sliderValue)
    {
        _musicGroup.audioMixer.SetFloat("volumeMusic", Mathf.Log10(sliderValue) * 20);
    }

    public void UpdateSFXMixer(float sliderValue)
    {
        _sfxGroup.audioMixer.SetFloat("volumeSFX", Mathf.Log10(sliderValue) * 20);
    }


    #endregion

    #region Private Methods

    private void UpdateAudioMixers()
    {
        float tmp = 0f;
        _masterGroup.audioMixer.GetFloat("volumeMaster", out tmp);
        _masterSlider.value = Mathf.Pow(10, tmp / 22);
        _musicGroup.audioMixer.GetFloat("volumeMusic", out tmp);
        _musicSlider.value = Mathf.Pow(10, tmp / 22);
        _sfxGroup.audioMixer.GetFloat("volumeSFX", out tmp);
        _sfxSlider.value = Mathf.Pow(10, tmp / 22);
    }

    public void ToggleValueChanged(Toggle change)
    {
        QualitySettings.SetQualityLevel(change.isOn ? 1 : 0);
    }

    #endregion

}
