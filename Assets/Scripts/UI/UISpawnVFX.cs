using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISpawnVFX : MonoBehaviour
{
    public GameObject _spawnVFX;
    [SerializeField] private AudioData _dataToPlay;

    public void SpawnVFX()
    {
        _spawnVFX.SetActive(true);
    }
    public void SpawnVFXAndSFX()
    {
        _spawnVFX.SetActive(true);
        AudioManager.PlayAudio(_dataToPlay);
        CameraShake.Shake();
    }
}
