using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;
using UnityEngine.InputSystem;

public class UITutorial : MonoBehaviour
{
    public enum TutorialType { Rotate, Shoot }

    [SerializeField] private GameObject _rotateCanvasGameObject;
    [SerializeField] private GameObject _shootCanvasGameObject;

    [SerializeField] private CanvasGroup _rotateTutorialCanvasGroup;
    [SerializeField] private CanvasGroup _shootTutorialCanvasGroup;

    [SerializeField] private float appearenceTime;
    [SerializeField] private float fadeTime;

    public static UnityAction StartTutorial;
    public static UnityAction<TutorialType> CloseTutorial;

    public static bool hasRotate;
    public static bool hasShoot;
    private bool rotateControl;
    private bool shootControl;

    #region Messages

    private void OnEnable()
    {
        StartTutorial += StartRotateTutorial;
        StartTutorial += StartShootTutorial;
        CloseTutorial += CloseTutorialMethod;
    }

    private void OnDisable()
    {
        StartTutorial -= StartRotateTutorial;
        StartTutorial -= StartShootTutorial;
        CloseTutorial -= CloseTutorialMethod;
    }

    private void Update()
    {
        if (hasRotate && !rotateControl)
        {
            UITutorial.CloseTutorial(TutorialType.Rotate);
            rotateControl = true;
        }
        if (hasShoot && !shootControl)
        {
            UITutorial.CloseTutorial(TutorialType.Shoot);
            shootControl = true;
        }
    }

    #endregion

    #region Private Methods

    private void StartRotateTutorial()
    {
        if (rotateControl)
            return;
        _rotateCanvasGameObject.SetActive(true);
        _rotateTutorialCanvasGroup.DOFade(1, appearenceTime);

    }

    private void StartShootTutorial()
    {
        if (shootControl)
            return;
        _shootCanvasGameObject.SetActive(true);
        _shootTutorialCanvasGroup.DOFade(1, appearenceTime);
    }

    private void CloseTutorialMethod(TutorialType tutorialType)
    {
        if (tutorialType == TutorialType.Rotate)
            _rotateTutorialCanvasGroup.DOFade(0, fadeTime)
                .OnComplete(()=> _rotateCanvasGameObject.SetActive(false));
        else
            _shootTutorialCanvasGroup.DOFade(0, fadeTime)
                .OnComplete(()=> _shootCanvasGameObject.SetActive(false));
    }

    #endregion

}
