using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIAnimationSelect : MonoBehaviour, IPointerEnterHandler
{
    [SerializeField] private Animation _animationComponenet;
    [SerializeField] private AnimationClip _animationEnterClip;

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (_animationComponenet.clip != _animationEnterClip)
            _animationComponenet.clip = _animationEnterClip;
        _animationComponenet.Play();
    }

}
