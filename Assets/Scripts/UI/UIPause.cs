using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIPause : MonoBehaviour
{
    #region Variables

    [SerializeField] private Button _pause;
    [SerializeField] private Button _resume;
    [SerializeField] private Button _mainMenu;

    [SerializeField] private GameObject _pauseScreen;
    [SerializeField] private GameObject _pauseButton;

    #endregion

    #region Messages

    private void OnEnable()
    {
        _pause.onClick.AddListener(Pause);
        _resume.onClick.AddListener(Resume);
        _mainMenu.onClick.AddListener(MainMenu);
    }

    private void OnDisable()
    {
        _pause.onClick.RemoveListener(Pause);
        _resume.onClick.RemoveListener(Resume);
        _mainMenu.onClick.RemoveListener(MainMenu);
    }

    #endregion

    #region Methods

    private void Pause()
    {
            GameManager.Pause();
            _pauseScreen.SetActive(true);
    }

    private void Resume()
    {
        GameManager.Resume();
    }

    private void MainMenu()
    {
        UITutorial.CloseTutorial(UITutorial.TutorialType.Rotate);
        UITutorial.CloseTutorial(UITutorial.TutorialType.Shoot);
        GameManager.SetState(GameState.Menu);
        LoadManager.UnloadScene(1, UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);
        UIMenuFlow.ToggleMenuButtons(true);
        UIMenuFlow.AnimatorToMenu();
        _pauseButton.SetActive(false);
        //UIFade.StartFade(UIFade.FadeType.FadeIn, RestartLevel);
    }

    private void RestartLevel()
    {

    }

    public void UpdateAudioStats()
    {
        UIOptions.UpdateValues();
    }

    #endregion
}
