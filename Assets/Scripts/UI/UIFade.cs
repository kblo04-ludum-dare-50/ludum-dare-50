using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;

public class UIFade : MonoBehaviour
{

    #region Variables

    public enum FadeType { FadeIn, FadeOut }

    [SerializeField] private CanvasGroup _canvsaGroupComponent;
    [SerializeField] private float _fadeTime;
    [SerializeField] private AnimationCurve _fadeCurve;
    [SerializeField] private GameObject _background;

    public static UnityAction<FadeType, System.Action> StartFade;

    #endregion

    #region Messages

    private void Start()
    {
        _background.SetActive(true);
    }

    private void OnEnable()
    {
        StartFade += Fade;
        Fade(FadeType.FadeOut, null);
    }

    private void OnDisable()
    {
        StartFade -= Fade;
    }

    #endregion

    #region Private Methods

    private void Fade(FadeType fadeType, System.Action callback)
    {
        if (fadeType == FadeType.FadeOut)
            _canvsaGroupComponent.DOFade(0, _fadeTime)
                .SetEase(_fadeCurve)
                .OnComplete(() => _background.SetActive(false));
        else
            _canvsaGroupComponent.DOFade(1, _fadeTime)
                .SetEase(_fadeCurve)
                .SetUpdate(true)
                .OnComplete(()=> Callback(callback));
    }

    private void Callback(System.Action callback)
    {
        if (callback != null)
            callback();
    }

    #endregion

}