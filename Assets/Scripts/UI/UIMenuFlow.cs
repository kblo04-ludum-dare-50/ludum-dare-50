using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Cinemachine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using TMPro;
using DG.Tweening;
using UnityEngine.Rendering;
public class UIMenuFlow : MonoBehaviour
{

    #region Variables
    [SerializeField] private GameObject _gameNameCanvas;
    [SerializeField] private Button _gameNameButton;
    [SerializeField] private CinemachineVirtualCamera _mainGameVirtualCamera;
    [SerializeField] private GameObject _startCamera;

    [SerializeField] private Button _ScoreBackButton;

    [SerializeField] private GameObject _scoreVirtualCamera;
    [SerializeField] private Button _optionsButton;
    [SerializeField] private Button _creditsButton;
    [SerializeField] private Button _optionsBackButton;
    [SerializeField] private Button _creditsBackButton;
    [SerializeField] private GameObject _pausecanvas;

    [SerializeField] private Animator _animatorComponent;

    [Header("Options Objects")]
    [SerializeField] private GameObject _mainOptionsObject;
    [SerializeField] private GameObject _menuOptionsObject;
    [SerializeField] private Animation _mainOptionsAnimationComponenet;
    [SerializeField] private Animation _menuOptionsAnimationComponenet;
    [SerializeField] private AnimationClip _mainOptionsOutClip;
    [SerializeField] private AnimationClip _mainOptionsInClip;
    [SerializeField] private AnimationClip _menuOptionsOutClip;
    [SerializeField] private AnimationClip _menuOptionsInClip;

    [Header("Credits Objects")]
    [SerializeField] private GameObject _mainCreditsObject;
    [SerializeField] private GameObject _menuCreditsObject;
    [SerializeField] private Animation _mainCreditsAnimationComponenet;
    [SerializeField] private Animation _menuCreditsAnimationComponenet;
    [SerializeField] private AnimationClip _mainCreditsOutClip;
    [SerializeField] private AnimationClip _mainCreditsInClip;
    [SerializeField] private AnimationClip _menuCreditsOutClip;
    [SerializeField] private AnimationClip _menuCreditsInClip;

    [Header("Deploy Objects")]
    [SerializeField] private Button _deployButton;
    [SerializeField] private GameObject adamPrefab;
    [SerializeField] private GameObject evePrefab;
    [SerializeField] private GameObject ufoObject;
    private GameObject instantiatedAdam;
    private GameObject instantiatedEve;
    [SerializeField] private float waitTimeToExplodeAdamAndEve;
    [SerializeField] private float waitTimeToStart;

    [Header("Score Objects")]
    [SerializeField] private GameObject _scorePanel;
    [SerializeField] private TextMeshProUGUI _scoreText;

    [Header("Audio Data")]
    [SerializeField] private AudioData _moonOpening;

    [SerializeField] private GameObject _introClouds;
    private Transform original;

    public static UnityAction<string> OpenScore;
    public static UnityAction CloseScore;
    public static UnityAction<bool> ToggleMenuButtons;

    public static UnityAction AnimatorToMenu;


    public static bool isOnScoreScreen;

    public static bool hasClickedOnScoreScreen;
    [SerializeField] private Volume damageVolume;
    #endregion

    #region Messages

    private void OnEnable()
    {
        _optionsButton.onClick.AddListener(OpenOptions);
        _optionsBackButton.onClick.AddListener(OpenMainMenu);
        _creditsBackButton.onClick.AddListener(OpenMainMenu);
        _creditsButton.onClick.AddListener(OpenCredits);
        _deployButton.onClick.AddListener(DeployAction);
        OpenScore += OpenScoreScreen;
        CloseScore += CloseScreenMethod;
        ToggleMenuButtons += ToggleButtons;
        _gameNameButton.onClick.AddListener(StartGame);
        AnimatorToMenu += GoToMenu;
        _ScoreBackButton.onClick.AddListener(CloseScreenMethod);


    }

    private void OnDisable()
    {
        _optionsButton.onClick.RemoveListener(OpenOptions);
        _optionsBackButton.onClick.RemoveListener(OpenMainMenu);
        _creditsBackButton.onClick.RemoveListener(OpenMainMenu);
        _creditsButton.onClick.RemoveListener(OpenCredits);
        _deployButton.onClick.RemoveListener(DeployAction);
        OpenScore -= OpenScoreScreen;
        CloseScore -= CloseScreenMethod;
        ToggleMenuButtons -= ToggleButtons;
        _gameNameButton.onClick.RemoveListener(StartGame);
        AnimatorToMenu -= GoToMenu;
        _ScoreBackButton.onClick.RemoveListener(CloseScreenMethod);

    }

    #endregion

    #region Private Methods
    private void GoToMenu()
    {
        _animatorComponent.SetTrigger("menu");
    }
    private void StartGame()
    {
        _animatorComponent.SetTrigger("start");
        _gameNameCanvas.GetComponent<CanvasGroup>().DOFade(0, 0.5f)
            .OnComplete(() => _gameNameCanvas.SetActive(false));
        _gameNameButton.gameObject.SetActive(false);
        StartCoroutine(DeactivateClouds());
    }
    IEnumerator DeactivateClouds()
    {
        yield return new WaitForSeconds(4f);
        _introClouds.SetActive(false);
        _startCamera.SetActive(false);
    }
    private void OpenOptions()
    {
        StartCoroutine(AnimationExecution(_mainOptionsAnimationComponenet, _mainOptionsOutClip, true));

        _animatorComponent.SetTrigger("options");

        StartCoroutine(AnimationExecution(_menuOptionsAnimationComponenet, _menuOptionsInClip));

    }
    private void OpenMainMenu()
    {
        if (_menuOptionsObject.activeInHierarchy)
        {
            StartCoroutine(AnimationExecution(_menuOptionsAnimationComponenet, _menuOptionsOutClip, true));
            StartCoroutine(AnimationExecution(_mainOptionsAnimationComponenet, _mainOptionsInClip));
        }
        if (_menuCreditsObject.activeInHierarchy)
        {
            StartCoroutine(AnimationExecution(_menuCreditsAnimationComponenet, _menuCreditsOutClip, true));
            StartCoroutine(AnimationExecution(_mainCreditsAnimationComponenet, _mainCreditsInClip));
        }
  

        _animatorComponent.SetTrigger("menu");

    }
    private void OpenCredits()
    {
        StartCoroutine(AnimationExecution(_menuCreditsAnimationComponenet, _menuCreditsInClip));

        _animatorComponent.SetTrigger("credits");

        StartCoroutine(AnimationExecution(_mainCreditsAnimationComponenet, _mainCreditsOutClip));
    }
    private void DeployAction()
    {
        ToggleButtons(false);
        _animatorComponent.SetTrigger("deploy");
        AudioManager.PlayAudio(_moonOpening);
        LoadManager.LoadScene(1, LoadSceneMode.Additive);

    }

    private void ToggleButtons(bool toggle)
    {
        _deployButton.interactable = toggle;
        _optionsButton.interactable = toggle;
        _creditsButton.interactable = toggle;

    }

    private void OpenScoreScreen(string playerScore)
    {
        _scorePanel.GetComponent<CanvasGroup>().DOFade(1, 0.5f);

        isOnScoreScreen = true;
        _scoreVirtualCamera.SetActive(true);
        _scorePanel.SetActive(true);
        _mainGameVirtualCamera.Priority = 10;
        _scoreText.text = "Score: "+  playerScore;
        _pausecanvas.GetComponent<CanvasGroup>().DOFade(1, 0.5f)
            .SetUpdate(true)
            .OnComplete(()=> _pausecanvas.SetActive(false));
        DOTween.To(() => damageVolume.weight, x => damageVolume.weight = x, 0, 1f)
            .SetEase(Ease.InOutSine)
            .SetUpdate(true);
    }

    private void Update()
    {
        if (GameManager.State != GameState.Gameplay)
            return;
        damageVolume.weight = RegionManager.worldAngleDanger;
    }

    IEnumerator StartGameplayRoutine()
    {
        yield return new WaitForSeconds(waitTimeToExplodeAdamAndEve);
        // Desaparece com adao e eva e VFX de Explos�o

        yield return new WaitForSeconds(waitTimeToStart);
        // Action para come�ar o jogo
        UITutorial.StartTutorial();

    }
    IEnumerator AnimationExecution(Animation animationComponent, AnimationClip animationClip, bool closeOnFinish = false)
    {

        animationComponent.clip = animationClip;
        animationComponent.gameObject.transform.GetChild(0).gameObject.SetActive(true);
        animationComponent.Play();
        yield return new WaitUntil(() => !animationComponent.isPlaying);
        if (closeOnFinish)
            animationComponent.gameObject.transform.GetChild(0).gameObject.SetActive(false);
    }

    private void CloseScreenMethod()
    {
        _animatorComponent.SetTrigger("menu");
        _scorePanel.GetComponent<CanvasGroup>().DOFade(0, 0.5f)
            .OnComplete(() => FinishClose());
    }

    private void FinishClose()
    {
        _scoreVirtualCamera.SetActive(false);
        _scorePanel.SetActive(false);
        _pausecanvas.SetActive(false);
        isOnScoreScreen = false;
        hasClickedOnScoreScreen = false;
        LoadManager.UnloadScene(1, UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);
        ToggleButtons(true);
    }

    #endregion

    #region Public Methods
    public void DeactiveObject()
    {
        _introClouds.SetActive(false);
    }
    public void ChangeToGameplayCamera()
    {
        try
        {
            _mainGameVirtualCamera = FindObjectOfType<CameraController>().GetComponent< CinemachineVirtualCamera>();
            _mainGameVirtualCamera.Priority = 999;
            //original = _mainGameVirtualCamera.LookAt;
            //_mainGameVirtualCamera.LookAt = ufoObject.transform;
            //_mainGameVirtualCamera.GetCinemachineComponent<CinemachineOrbitalTransposer>().m_YawDamping = 2f;
            GameManager.SetState(GameState.Gameplay);
            _pausecanvas.SetActive(true);
            _pausecanvas.GetComponent<CanvasGroup>().DOFade(1, 0.5f)
                .SetUpdate(true);
        }
        catch
        {
            Debug.LogError("Gameplay Camera not found");
        }
    }
    
    public void SpawnAdamAndEve()
    {
        //instantiatedAdam = Instantiate(adamPrefab, ufoObject.transform);
        //instantiatedEve = Instantiate(evePrefab, ufoObject.transform);
        //_mainGameVirtualCamera.LookAt = original;
        //_mainGameVirtualCamera.GetCinemachineComponent<CinemachineOrbitalTransposer>().m_YawDamping = 0;
        StartCoroutine(StartGameplayRoutine());
        SpaceShip.EnableVisual();
    }

    #endregion

    [ContextMenu("Teste")]
    private void teste()
    {
        OpenScore("testeee");
    }
}