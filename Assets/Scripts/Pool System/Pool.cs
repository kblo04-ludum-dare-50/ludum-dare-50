using System.Collections.Generic;
using UnityEngine;

namespace KB.PoolSystem
{
    public class Pool<T> where T : Component
    {
        public Transform Parent { get; private set; }
        private readonly Queue<T> components = new Queue<T>();
        private GameObject _prefab;

        public void SetPool(int poolSize, GameObject prefab, Transform parent = null)
        {
            Parent = parent;
            _prefab = prefab;

            for (int i = 0; i < poolSize; i++)
            {
                T component = CreatePoolObject();

                if (component == null)
                {
                    Debug.LogError($"Could not create a pool from {prefab.name}.");
                    return;
                }

                components.Enqueue(component);
            }
        }

        private T CreatePoolObject()
        {
            GameObject go = Object.Instantiate(_prefab, Parent);
            go.SetActive(false);

            if (!go.TryGetComponent(out T component))
            {
                Debug.Log($"Could not find the component of type {typeof(T).Name}.");
                Object.Destroy(go);
                return null;
            }

            return component;
        }

        public T GetObject()
        {
            T component;

            if (components.Count == 0)
                component = CreatePoolObject();
            else
                component = components.Dequeue();

            component.gameObject.SetActive(true);
            return component;
        }

        public void RecycleObject(T component)
        {
            component.gameObject.SetActive(false);
            components.Enqueue(component);
        }
    }
}