using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Objects/Audio System/Audio Manager Data")]
public class AudioManagerData : ScriptableObject
{
    public int poolSize;
    public GameObject audioSourcePrefab;
}
