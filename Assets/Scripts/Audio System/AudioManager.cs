using UnityEngine;
using KB.Utils;
using KB.PoolSystem;
using System.Collections;
using UnityEngine.Events;
using System;

public class AudioManager : DontDestroyOnLoad
{
    #region Constants

    private const string DataPath = "Audio Manager Data";

    #endregion

    #region Variables

    private static AudioManagerData data;
    private static Pool<AudioSource> pool = new Pool<AudioSource>();

    private static UnityEvent<AudioSource, Action> OnRequestRecycle = new UnityEvent<AudioSource, Action>();

    #endregion

    #region Iniialization

    [RuntimeInitializeOnLoadMethod]
    private static void Init() => CreateSingleton(typeof(AudioManager), name: "[Audio Manager]");

    #endregion

    #region Messages

    protected override void Awake()
    {
        base.Awake();

        data = Resources.Load<AudioManagerData>(DataPath);
        pool.SetPool(data.poolSize, data.audioSourcePrefab, transform);
    }

    private void OnEnable()
    {
        OnRequestRecycle.AddListener(RequestRecycle);
    }

    private void OnDisable()
    {
        OnRequestRecycle.RemoveListener(RequestRecycle);
    }

    #endregion

    #region Methods

    #region Play

    public static void PlayAudio(AudioData audioData, Transform transform = null)
    {
        AudioSource audioSource = GetAudioSource(audioData, transform);
        OnRequestRecycle.Invoke(audioSource, null);
    }

    public static void PlayAudioLoop(AudioData audioData, ref Action stopAction, Transform transform = null)
    {
        AudioSource audioSource = GetAudioSource(audioData, transform);
        OnRequestRecycle.Invoke(audioSource, stopAction);
        stopAction += audioSource.Stop;
    }

    #endregion

    #region Setup

    private static AudioSource GetAudioSource(AudioData audioData, Transform transform)
    {
        AudioSource audioSource = pool.GetObject();
        SetAudioSource(audioSource, audioData);
        SetPosition(audioSource.transform, transform, audioData.setParent);
        audioSource.Play();
        return audioSource;
    }

    private static void SetAudioSource(AudioSource source, AudioData data)
    {
        source.clip = data.clip;
        source.outputAudioMixerGroup = data.mixerGroup;
        source.loop = data.loop;
        source.volume = data.volume;
        source.pitch = data.pitch;
        source.panStereo = data.stereoPan;
        source.spatialBlend = data.spatialBlend;
        source.minDistance = data.minDistance;
        source.maxDistance = data.maxDistance;
    }

    private static void SetPosition(Transform audioSource, Transform transform, bool setParent)
    {
        if (transform == null)
            return;

        if (setParent)
        {
            audioSource.SetParent(transform);
            audioSource.localPosition = Vector3.zero;
        }

        else
        {
            audioSource.position = transform.position;
        }
    }

    #endregion

    #region Recycle

    private void RequestRecycle(AudioSource audioSource, Action stopAction = null)
    {
        if (stopAction != null)
            StartCoroutine(RecycleAudioSource(audioSource, stopAction));
        else
            StartCoroutine(RecycleAudioSource(audioSource));
    }

    private IEnumerator RecycleAudioSource(AudioSource audioSource, Action stopAction = null)
    {
        yield return new WaitWhile(() => audioSource.isPlaying);

        if(stopAction != null)
            stopAction -= audioSource.Stop;

        if(audioSource.transform.parent != pool.Parent)
        {
            audioSource.transform.parent = pool.Parent;
            audioSource.transform.localPosition = Vector3.zero;
        }

        pool.RecycleObject(audioSource);
    }

    #endregion

    #endregion
}