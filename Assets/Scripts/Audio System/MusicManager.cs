using System;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    [SerializeField] private AudioLayer[] _layers;
    private float _lastAngle;

    private void Update()
    {
        if (RegionManager.worldAngleDanger == _lastAngle)
            return;

        _lastAngle = RegionManager.worldAngleDanger;

        foreach (AudioLayer layer in _layers)
            layer.SetVolume(_lastAngle);
    }

    [Serializable]
    private class AudioLayer
    {
        [SerializeField] private AudioSource _audioSource;
        [SerializeField, Range(0, 1)] private float _volume;
        [SerializeField, Range(0, 1)] private float _startFade;
        [SerializeField, Range(0, 1)] private float _endFade;

        public void SetVolume(float master)
        {
            float multiplier = (master - _startFade) / (_endFade - _startFade);
            _audioSource.volume = _volume * Mathf.Clamp01(multiplier);
        }
    }
}
