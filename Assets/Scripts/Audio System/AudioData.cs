using UnityEngine;
using UnityEngine.Audio;

[CreateAssetMenu(menuName = "Scriptable Objects/Audio System/Audio Data")]
public class AudioData : ScriptableObject
{
    [Header("Data")]
    public AudioClip clip;
    public AudioMixerGroup mixerGroup;

    [Header("General Settings")]
    public bool loop;
    [Range(0, 1)] public float volume = 1;
    [Range(-3, 3)] public float pitch = 1;
    [Range(-1, 1)] public float stereoPan;

    [Header("3D Settings")]
    [Range(0, 1)] public float spatialBlend;
    [Min(0)] public float minDistance = 1;
    [Min(0)] public float maxDistance = 100;

    [Header("Spawn Settings")]
    public bool setParent;
}
