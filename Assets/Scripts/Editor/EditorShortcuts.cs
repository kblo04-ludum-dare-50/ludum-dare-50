using UnityEditor;
using UnityEditor.SceneManagement;

public static class EditorShortcuts
{
    [MenuItem("Game/Test Game %T")]
    private static void TestGame()
    {
        EditorSceneManager.OpenScene("Assets/Scenes/Main Menu.unity", OpenSceneMode.Single);
        EditorApplication.EnterPlaymode();
    }
}
