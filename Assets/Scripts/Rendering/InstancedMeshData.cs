using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering;

[CreateAssetMenu(menuName = "Scriptable Objects/Rendering")]
public class InstancedMeshData : ScriptableObject
{
    #region Variables

    [SerializeField] private Mesh _mesh;
    [SerializeField] private Material _material;
    [SerializeField] private ShadowCastingMode _shadowCasting;
    [SerializeField] private bool _receiveShadows;
    [SerializeField] private FloatProperty[] floatProperties;
    [SerializeField] private Vector4Property[] vector4Properties;

    private MaterialPropertyBlock _propertyBlock;

    #endregion

    #region Properties

    public Mesh Mesh => _mesh;
    public Material Material => _material;
    public ShadowCastingMode ShadowCasting => _shadowCasting;
    public bool ReceiveShadows => _receiveShadows;

    public MaterialPropertyBlock PropertyBlock
    {
        get
        {
            if (_propertyBlock == null)
                _propertyBlock = new MaterialPropertyBlock();

            return _propertyBlock;
        }
    }

    #endregion

    #region Constants

    public const int MaxInstances = 1023;

    #endregion

    #region Methods

    public void Init()
    {
        foreach (FloatProperty property in floatProperties)
            PropertyBlock.SetFloatArray(property.Id, Enumerable.Repeat(property.Default, MaxInstances).ToArray());

        foreach (Vector4Property property in vector4Properties)
            PropertyBlock.SetVectorArray(property.Id, Enumerable.Repeat(property.Default, MaxInstances).ToArray());
    }

    public void SetProperty(int id, float value, int index)
    {
        float[] values = PropertyBlock.GetFloatArray(id);
        values[index] = value;
        PropertyBlock.SetFloatArray(id, values);
    }

    public void SetProperty(int id, Vector4 value, int index)
    {
        PropertyBlock.GetVectorArray(id)[index] = value;
    }

    public void SetProperty(int id, Vector4[] values) => PropertyBlock.SetVectorArray(id, values);

    #endregion

    #region Structs

    [Serializable]
    private struct FloatProperty
    {
        [SerializeField] private string _name;
        [SerializeField] private float _default;

        public int Id => Shader.PropertyToID(_name);
        public float Default => _default;
    }

    [Serializable]
    private struct Vector4Property
    {
        [SerializeField] private string _name;
        [SerializeField] private Vector4 _default;

        public int Id => Shader.PropertyToID(_name);
        public Vector4 Default => _default;
    }

    #endregion
}
