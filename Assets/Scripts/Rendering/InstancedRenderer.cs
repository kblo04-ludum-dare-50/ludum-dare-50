using KB.Utils;
using System.Collections.Generic;
using UnityEngine;

public class InstancedRenderer : DontDestroyOnLoad
{
    #region Variables

    private static readonly Dictionary<InstancedMeshData, Renderer> _renderers = new Dictionary<InstancedMeshData, Renderer>();

    #endregion

    #region Initialization

    [RuntimeInitializeOnLoadMethod]
    private static void Init() => CreateSingleton(typeof(InstancedRenderer));

    #endregion

    #region Messages

    private void Update()
    {
        foreach (Renderer renderer in _renderers.Values)
            renderer.Update();
    }

    #endregion

    #region Methods

    public static void AddRenderer(InstancedMeshData data, Transform transform)
    {
        if (_renderers.Count == InstancedMeshData.MaxInstances)
        {
            Debug.LogError($"Max render instances reached for {data.name}.");
            return;
        }

        if (!_renderers.TryGetValue(data, out Renderer renderer))
        {
            renderer = new Renderer(data);
            _renderers.Add(data, renderer);
        }

        renderer.AddRenderer(transform);
    }

    public static void RemoveRenderer(InstancedMeshData data, Transform transform)
    {
        if (!_renderers.TryGetValue(data, out Renderer renderer))
        {
            Debug.LogError($"Could not find any instanced renderer for {data.name}.");
            return;
        }

        renderer.RemoveRenderer(transform);
    }

    public static void UpdateInstances(InstancedMeshData data, Transform[] transforms)
    {
        if (!_renderers.TryGetValue(data, out Renderer renderer))
        {
            Debug.LogError($"Could not find any instanced renderer for {data.name}.");
            return;
        }

        renderer.UpdateInstances(transforms);
    }

    #region Set Property

    public static void SetProperty(int id, float value, InstancedMeshData data, Transform transform)
    {
        if (!_renderers.TryGetValue(data, out Renderer renderer))
        {
            Debug.LogError($"Could not find any instanced renderer for {data.name}.");
            return;
        }

        renderer.SetProperty(id, value, transform);
    }

    public static void SetProperty(int id, Vector4 value, InstancedMeshData data, Transform transform)
    {
        if (!_renderers.TryGetValue(data, out Renderer renderer))
        {
            Debug.LogError($"Could not find any instanced renderer for {data.name}.");
            return;
        }

        renderer.SetProperty(id, value, transform);
    }

    #endregion

    public static void UpdatePosition(InstancedMeshData data, Transform transform)
    {
        if(!_renderers.TryGetValue(data, out Renderer renderer))
        {
            Debug.LogError($"Could not find any instanced renderer for {data.name}.");
            return;
        }

        renderer.UpdatePosition(transform);
    }

    public static void UpdatePositions(InstancedMeshData data, Transform[] transforms)
    {
        if (!_renderers.TryGetValue(data, out Renderer renderer))
        {
            Debug.LogError($"Could not find any instanced renderer for {data.name}.");
            return;
        }

        renderer.UpdatePositions(transforms);
    }

    #endregion

    #region Classes

    private class Renderer
    {
        private readonly InstancedMeshData _data;
        private readonly List<Transform> _transforms = new List<Transform>();
        private readonly List<Matrix4x4> _matrices = new List<Matrix4x4>();

        public Renderer(InstancedMeshData data)
        {
            _data = data;
            _data.Init();
        }

        public void AddRenderer(Transform transform)
        {
            _transforms.Add(transform);
            _matrices.Add(transform.localToWorldMatrix);
        }

        public void RemoveRenderer(Transform transform)
        {
            _matrices.Remove(transform.localToWorldMatrix);
            _transforms.Remove(transform);
        }

        public void UpdateInstances(Transform[] transforms)
        {
            _matrices.Clear();

            foreach (var transform in transforms)
            {
                _matrices.Add(transform.localToWorldMatrix);
            }
        }

        public void SetProperty(int id, float value, Transform transform)
        {
            _data.SetProperty(id, value, _transforms.IndexOf(transform));
        }

        public void SetProperty(int id, Vector4 value, Transform transform)
        {
            _data.SetProperty(id, value, _transforms.IndexOf(transform));
        }

        public void UpdatePosition(Transform transform) => _matrices[_transforms.IndexOf(transform)] = transform.localToWorldMatrix;

        public void UpdatePositions(Transform[] transforms)
        {
            for (int i = 0; i < transforms.Length; i++)
            {
                _matrices[i] = transforms[i].localToWorldMatrix;
            }
        }

        public void Update()
        {
            Graphics.DrawMeshInstanced(_data.Mesh, 0, _data.Material, _matrices, _data.PropertyBlock, _data.ShadowCasting, _data.ReceiveShadows);
        }
    }

    #endregion
}