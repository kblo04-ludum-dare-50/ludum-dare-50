using UnityEngine;


[RequireComponent(typeof(Rigidbody))]
public class DynamicObj : MonoBehaviour
{
    public PhysicMaterial physicsMaterial;
    protected Rigidbody rb;
  

    protected virtual void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void OnEnable()
    {
        GameManager.OnStateChanged += StateChanged;
    }

    private void OnDisable()
    {
        GameManager.OnStateChanged -= StateChanged;
    }

    private void StateChanged(GameState gameState)
    {
        if (gameState != GameState.Gameplay)
        {
            DestroySelf();
        }
    }

    protected void UpdatePhysics(PhysicsData physicsData)
    {
        if (physicsData)
        {
            rb.mass = physicsData.weight;
            rb.drag = physicsData.drag;
            rb.angularDrag = physicsData.angularDrag;
        }
    }

    internal virtual void DestroySelf()
    {
        //Destroy(this.gameObject);
    }
}
