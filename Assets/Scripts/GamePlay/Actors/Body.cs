using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using KB.PoolSystem;
using UnityEngine.Events;

[RequireComponent(typeof(NavMeshAgent))]
public class Body : DynamicObj
{
    public BodyData data;
    private Pool<Body> myPool;
    private Transform poolParent;
    private Region region;

    private NavMeshAgent agent;
    private float timeToReposition;
    private float lastRepositionTime;
    Vector3 destination = Vector3.zero;
    Vector3 localDestination = Vector3.zero;
    private bool active = false;

    private bool isDestroyed = false;

    public GameObject[] graphics;
    private int _shaderOffsetID = Shader.PropertyToID("_JumpOffset");
    private int graphicsIndex;

    protected override void Awake()
    {
        base.Awake();
        agent = GetComponent<NavMeshAgent>();
        agent.updateRotation = false;
        agent.updatePosition = false;
    }

    public void InitializeBody(Region _region, Pool<Body> myPool, Transform poolParent)
    {
        if (data.FXData.spawnAudioData) AudioManager.PlayAudio(data.FXData.spawnAudioData, transform);
        if (data.FXData.spawnVfxData) VFXManager.PlayVFX(data.FXData.spawnVfxData, transform);
        region = _region;
        isDestroyed = false;
        this.myPool = myPool;
        this.poolParent = poolParent;
        timeToReposition = GetNewRepositionTime();
        agent.enabled = true;
        agent.speed = Random.Range(data.moveSpeedRange.x, data.moveSpeedRange.y);
        UpdatePhysics(data.physicsData);
        active = true;

        graphicsIndex = Random.Range(0, 2);
        graphics[graphicsIndex].SetActive(true);
        graphics[graphicsIndex].GetComponent<Renderer>().material.SetFloat(_shaderOffsetID, Random.value);
    }

    private void Update()
    {
        if (!active)
            return;
        RepositioningHandler();
    }

    private void FixedUpdate()
    {
        if (!active)
            return;
        if (agent.desiredVelocity != Vector3.zero)
        {
            //agent.SetDestination(region.transform.TransformPoint(localDestination));
            rb.MovePosition(rb.position + agent.desiredVelocity * Time.deltaTime);
        }
    }

    private void LateUpdate()
    {
        if (agent.desiredVelocity != Vector3.zero)
        {
            Quaternion rot = Quaternion.LookRotation(new Vector3(agent.desiredVelocity.x, 0f, agent.desiredVelocity.z));
            transform.rotation = Quaternion.Slerp(transform.rotation, rot, 30f * Time.deltaTime);
        }
    }

    private void RepositioningHandler()
    {
        if (Time.time >= lastRepositionTime + timeToReposition)
        {
            MoveToNewPosition();
            lastRepositionTime = Time.time;
            timeToReposition = GetNewRepositionTime();
        }
    }

    private void MoveToNewPosition()
    {
        if (region)
        {
            destination = region.SelectPointInArea();
            localDestination = region.transform.InverseTransformPoint(destination);
            agent.SetDestination(destination);
        }
        else
            agent.SetDestination(transform.position + transform.forward * 5f + transform.right * 5f);
    }

    private void OnDrawGizmos()
    {
        //Gizmos.color = Color.yellow;
        //Gizmos.DrawLine(transform.position, destination);
        //Gizmos.DrawSphere(destination, 0.3f);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Body"))
        {
            Body otherBody = other.GetComponent<Body>();
            otherBody.isDestroyed = true;
            if (isDestroyed) return;
            Vector3 eventPos = transform.position;// transform.position + ((other.gameObject.transform.position - transform.position) * .5f);
            region.SpawnBuilding(eventPos);
            otherBody.DestroySelfWithoutSound();
            DestroySelfWithoutSound();
        }
    }

    internal override void DestroySelf()
    {
        //PlaySFX
        if (data.FXData.destroyAudioData)
            AudioManager.PlayAudio(data.FXData.destroyAudioData, transform);
        if (data.FXData.destroyVfxData)
            VFXManager.PlayVFX(data.FXData.destroyVfxData, transform);
        agent.enabled = false;
        graphics[graphicsIndex].SetActive(false);
        active = false;
        myPool.RecycleObject(this);
        transform.SetParent(poolParent, false);
        //InstancedRenderer.RemoveRenderer(instanceMeshData, transform);
        //DynamicObjManager.activeBodies.Remove(transform);
        //InstancedRenderer.UpdateInstances(instanceMeshData, DynamicObjManager.activeBodies.ToArray());
    }

    public void DestroySelfWithoutSound()
    {
        if (data.FXData.destroyVfxData)
            VFXManager.PlayVFX(data.FXData.destroyVfxData, transform);
        agent.enabled = false;
        active = false;
        myPool.RecycleObject(this);
        transform.SetParent(poolParent, false);
    }

    #region Utils
    private float GetNewRepositionTime()
    {
        return Random.Range(data.repositionTimeRange.x, data.repositionTimeRange.y);
    }
    #endregion
}
