using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using KB.PoolSystem;

public class Building : DynamicObj
{
    private int currentBuildingLevel;
    private float currentGrowthPercent;
    private Pool<Building> myPool;
    private Transform poolParent;
    private bool active = false;

    public BuildingData buildingData;
    [SerializeField] private Transform graphicsContainer;
    [SerializeField] private Transform collidersContainer;

    private static UnityEvent<Body, Building> OnBodyBuildingCollision = new UnityEvent<Body, Building>();
    private IEnumerator animationRoutine;

    private void Update()
    {
        if (!active)
            return;
        GrowthHandler();
    }

    public void InitializeBuilding(Pool<Building> myPool, Transform poolParent)
    {
        if (buildingData.specificBuildingData[currentBuildingLevel].FXData.spawnAudioData)
            AudioManager.PlayAudio(buildingData.specificBuildingData[currentBuildingLevel].FXData.spawnAudioData, transform);
        if (buildingData.specificBuildingData[currentBuildingLevel].FXData.spawnVfxData)
            VFXManager.PlayVFX(buildingData.specificBuildingData[currentBuildingLevel].FXData.spawnVfxData, transform);
        this.myPool = myPool;
        this.poolParent = poolParent;
        currentBuildingLevel = 0;
        UpdateBuildingLevel(currentBuildingLevel);
        active = true;

        AnimateBuilding();
    }

    #region Growth
    private void GrowthHandler()
    {
        currentGrowthPercent += buildingData.specificBuildingData[currentBuildingLevel].growthRate * Time.deltaTime;
        ValidateGrowth();
    }

    public void AddGrowth(float _value)
    {
        currentGrowthPercent += _value;
        ValidateGrowth();
    }

    private void ValidateGrowth()
    {
        if (currentGrowthPercent >= 100f)
        {
            currentBuildingLevel++;
            currentBuildingLevel = Mathf.Clamp(currentBuildingLevel, 0, graphicsContainer.childCount - 1);
            UpdateBuildingLevel(currentBuildingLevel);
        }
    }
    #endregion

    #region Building Level
    public void UpdateBuildingLevel(int _buildingLevel)
    {
        currentGrowthPercent = 0f;
        UpdateGraphics(_buildingLevel);
        UpdatePhysics(buildingData.specificBuildingData[_buildingLevel].physicsData);
        AnimateBuilding();
    }

    private void UpdateGraphics(int _buildingLevel)
    {
        for (int i = 0; i <= graphicsContainer.childCount - 1; i++)
        {
            if (i != _buildingLevel)
            {
                graphicsContainer.GetChild(i).gameObject.SetActive(false);
                collidersContainer.GetChild(i).gameObject.SetActive(false);
            }
            else
            {
                graphicsContainer.GetChild(i).gameObject.SetActive(true);
                collidersContainer.GetChild(i).gameObject.SetActive(true);
            }
        }
        if (buildingData.specificBuildingData[currentBuildingLevel].FXData.spawnVfxData)
            VFXManager.PlayVFX(buildingData.specificBuildingData[currentBuildingLevel].FXData.spawnVfxData, transform);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Body"))
        {
            AddGrowth(buildingData.specificBuildingData[currentBuildingLevel].growthPerPeople);
            Body b = other.GetComponent<Body>();
            OnBodyBuildingCollision?.Invoke(b, this);
            b.DestroySelfWithoutSound();
            return;
        }
        else if (other.CompareTag("Building"))
        {
            //Buildings collides
        }
    }

    internal override void DestroySelf()
    {
        if (buildingData.specificBuildingData[currentBuildingLevel].FXData.destroyAudioData)
            AudioManager.PlayAudio(buildingData.specificBuildingData[currentBuildingLevel].FXData.destroyAudioData, transform);
        if (buildingData.specificBuildingData[currentBuildingLevel].FXData.destroyVfxData)
            VFXManager.PlayVFX(buildingData.specificBuildingData[currentBuildingLevel].FXData.destroyVfxData, transform);
        active = false;
        myPool.RecycleObject(this);
        transform.SetParent(poolParent, false);
    }

    private void AnimateBuilding()
    {
        if(animationRoutine != null)
        {
            StopCoroutine(animationRoutine);
        }
        animationRoutine = AnimateSpawn(0.6f, new Vector3(0f, -10f, 0f));
        StartCoroutine(animationRoutine);
    }

    private IEnumerator AnimateSpawn(float _time, Vector3 _offSet)
    {
        graphicsContainer.localPosition = _offSet;
        float timeStep = 0f;
        while(timeStep < _time)
        {
            graphicsContainer.localPosition = Vector3.Lerp(_offSet, Vector3.zero, timeStep / _time);
            timeStep += Time.deltaTime;
            yield return null;
        }
        animationRoutine = null;
    }
    #endregion
}
