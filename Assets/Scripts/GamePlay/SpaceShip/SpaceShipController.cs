using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class SpaceShipController : MonoBehaviour
{
    public SpaceShip spaceShip;
    private void OnEnable()
    {
        InputManager.Fire += Laser;
    }
    private void OnDisable()
    {
        InputManager.Fire -= Laser;
    }

    private void Laser()
    {
        if (GameManager.State != GameState.Gameplay || GameManager.IsPaused)
            return;
        Vector3 mousePosition = Mouse.current.position.ReadValue();
        Ray cameraRay = Camera.main.ScreenPointToRay(mousePosition);
        RaycastHit hit = new RaycastHit();
        Physics.Raycast(cameraRay, out hit, 1000, spaceShip.data.groundLayerMask);
        if (hit.collider)
        {
            Vector3 localPosition = transform.root.InverseTransformPoint(hit.point);
            spaceShip.ShotOn(localPosition);
        }
    }
}
