using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

public class SpaceShip : MonoBehaviour
{
    public Transform world;
    public SpaceShipData data;
    public Transform shipVisual;
    private Queue<Vector3> _positionsToDestroy = new Queue<Vector3>();
    private bool _inAction = false;
    private RaycastHit[] _collidersToDestroy;
    private bool inRotation = false;

    private static event UnityAction OnEnableVisuals;

    private void OnEnable()
    {
        OnEnableVisuals += EnableVisuals;
    }

    private void OnDisable()
    {
        OnEnableVisuals -= EnableVisuals;
    }

    public static void EnableVisual() => OnEnableVisuals?.Invoke();

    private void EnableVisuals()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(true);
        }
    }

    public void OldShotOn(Vector3 newLocalPosition)
    {
        newLocalPosition.y = data.shipHeight;
        _positionsToDestroy.Enqueue(newLocalPosition);
        if (!_inAction)
        {
            DestroyNextTarget();
        }
    }

    private void DestroyNextTarget()
    {
        if (_positionsToDestroy.Count < 1)
        {
            _inAction = false;
            return;
        }
        _inAction = true;
        Vector3 nextTarget = _positionsToDestroy.Dequeue();
        if (goToNextTargetCoroutine != null)
        {
            StopCoroutine(goToNextTargetCoroutine);
        }
        goToNextTargetCoroutine = GoToNextTargetCoroutine(nextTarget);
        StartCoroutine(goToNextTargetCoroutine);
    }

    public void ShotOn(Vector3 newLocalPosition)
    {
        newLocalPosition.y = data.shipHeight;
        DestroyTarget(newLocalPosition);
    }

    private void DestroyTarget(Vector3 nextTarget)
    {
        if (goToNextTargetCoroutine != null)
        {
            StopCoroutine(goToNextTargetCoroutine);
        }
        goToNextTargetCoroutine = GoToNextTargetCoroutine(nextTarget);
        StartCoroutine(goToNextTargetCoroutine);
    }

    private IEnumerator goToNextTargetCoroutine;
    private IEnumerator GoToNextTargetCoroutine(Vector3 nextTarget)
    {
        float lerpValue = 0;
        Vector3 startPosition = transform.localPosition;
        var targetDirection = (nextTarget + (transform.forward * data.rotationTargetVirtualDistance)) - transform.position;
        var nextTargetRotation = targetDirection - (transform.up * data.lookDownHeight);
        inRotation = true;
        TryToLookAtDirection(nextTarget - transform.position);
        RotateToMove(Quaternion.LookRotation(nextTargetRotation), data.lookDownSpeed, true);
        while (Vector3.Distance(transform.localPosition, nextTarget) > data.distanceFromTargetToShot)
        {
            lerpValue += Time.deltaTime * data.shipSpeed;
            transform.localPosition = Vector3.Slerp(transform.localPosition, nextTarget, lerpValue * Time.deltaTime);
            yield return null;
        }
        //RotateToMove(Quaternion.Euler(0, transform.rotation.eulerAngles.y, 0), data.lookUpSpeed);
        StartCoroutine(ShotCoroutine());
        //DestroyNextTarget();
    }

    IEnumerator ShotCoroutine()
    {
        PlayShotVFX();
        PlayShotSFX();
        yield return new WaitForSeconds(data.shotDuration);
        PlayShotCameraShake();
        PlayShotImpactSFX();
        Shot();
        yield return new WaitForSeconds(data.waitDuration);
    }

    void TryToLookAtDirection(Vector3 target)
    {
        if (inRotation)
            return;
        shipVisual.LookAt(target);
    }

    void RotateToMove(Quaternion rotation, float speed, bool lookUp = false)
    {
        StopRotation();
        rotateToMoveCoroutine = RotateToMoveCoroutine(rotation, speed, lookUp);
        StartCoroutine(rotateToMoveCoroutine);
    }

    void StopRotation()
    {
        if (rotateToMoveCoroutine != null)
            StopCoroutine(rotateToMoveCoroutine);
    }

    private IEnumerator rotateToMoveCoroutine;
    private IEnumerator RotateToMoveCoroutine(Quaternion rotation, float speed, bool lookUp = false)
    {
        float lerpValue = 0;
        Quaternion startRotation = shipVisual.localRotation;
        while (lerpValue <= 1)
        {
            lerpValue += Time.deltaTime * speed;
            shipVisual.localRotation = Quaternion.Lerp(startRotation, rotation, lerpValue);
            yield return null;
        }
        if (lookUp)
        {
            RotateToMove(Quaternion.Euler(0, shipVisual.localRotation.eulerAngles.y, 0), data.lookUpSpeed);
        }
        else
        {
            inRotation = false;
        }
    }

    private void Shot()
    {
        Vector3 explosionPosition = world.TransformPoint(transform.localPosition/* - (transform.up * shipHeight)*/);
        _collidersToDestroy = Physics.SphereCastAll(transform.position, data.explosionRadius, -transform.up, data.shipHeight, data.laserLayerMask, QueryTriggerInteraction.Collide);
        if (_collidersToDestroy == null || _collidersToDestroy.Length < 1)
            return;
        for (int i = 0; i < _collidersToDestroy.Length; i++)
        {
            if (_collidersToDestroy[i].collider.CompareTag("Building") || _collidersToDestroy[i].collider.CompareTag("Body"))
            {
                if (_collidersToDestroy[i].collider.TryGetComponent<DynamicObj>(out var dynamicObj))
                {
                    dynamicObj.DestroySelf();
                }
            }
        }
    }

    private void PlayShotVFX()
    {
        if (data.shotVfxData)
            VFXManager.PlayVFX(data.shotVfxData, transform, data.shotVfxOffSet);
    }
    private void PlayShotCameraShake()
    {
        CameraShake.Shake();
    }

    private void PlayShotSFX()
    {
        if (data.shotAudioData)
            AudioManager.PlayAudio(data.shotAudioData, transform);
    }

    private void PlayShotImpactSFX()
    {
        if (data.shotImpactAudioData)
            AudioManager.PlayAudio(data.shotImpactAudioData, transform);
    }

    /*private void OnDrawGizmos()
    {
        if (!gizmosOn)
            return;
        Gizmos.color = Color.red;
        for (int i = 0; i < _positionsToDestroy.Count; i++)
        {
            var newValue = _positionsToDestroy.Dequeue();
            Gizmos.DrawSphere(transform.root.TransformPoint(newValue), 100);
            _positionsToDestroy.Enqueue(newValue);
        }
    }*/
}
