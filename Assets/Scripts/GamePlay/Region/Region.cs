using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Region : MonoBehaviour
{
    public RegionSpawnData data;
    [SerializeField] private RegionArea[] _areas;
    private float _lastSpawn;

    private void Start()
    {
        if (_areas == null || _areas.Length < 1)
            _areas = GetComponentsInChildren<RegionArea>();
    }

    private void Update()
    {
        if (GameManager.IsPaused || GameManager.State != GameState.Gameplay)
            return;
        if (_lastSpawn <= 0)
        {
            SpawnBody();
            _lastSpawn = GetCurveValue();
        }
        else
        {
            _lastSpawn -= Time.deltaTime;
        }
    }

    private float GetCurveValue()
    {
        float gameTime = RegionManager.GetGameTime();
        float result = data.spawnRate.Evaluate(gameTime);
        return result;
    }

    public void SpawnBody()
    {
        if (_areas == null || _areas.Length < 1)
            return;
        //Body newPeople = Instantiate(data.peopleToSpawn, SelectPointInArea(), Quaternion.Euler(Quaternion.identity.x, Random.Range(0, 360f), Quaternion.identity.z)).GetComponent<Body>();
        DynamicObjManager.InstantiateBody(SelectPointInArea(), this, Quaternion.Euler(Quaternion.identity.x, Random.Range(0, 360f), Quaternion.identity.z));
        //newPeople.InitializeBody(this);
    }

    public void SpawnBuilding(Vector3 _pos)
    {
        //Check for close buildings
        //Building building = Instantiate(data.buildingPrefab, _pos, Quaternion.Euler(Quaternion.identity.x, Random.Range(0, 360f), Quaternion.identity.z), transform).GetComponent<Building>();
        //building.InitializeBuilding();
        DynamicObjManager.InstantiateBuilding(_pos, this, Quaternion.Euler(Quaternion.identity.x, Random.Range(0, 360f), Quaternion.identity.z));
    }

    public Vector3 SelectPointInArea()
    {
        if (_areas == null || _areas.Length < 1)
            return Vector3.zero;
        int selectedArea = 0;
        if (_areas.Length > 1)
        {
            float totalArea = 0;
            for (int i = 0; i < _areas.Length; i++)
            {
                totalArea += _areas[i].areaRadius;
            }
            float selectedAreaRadius = Random.Range(0, totalArea);
            totalArea = 0;
            for (int i = 0; i < _areas.Length; i++)
            {
                totalArea += _areas[i].areaRadius;
                if (totalArea > selectedAreaRadius)
                {
                    selectedArea = i;
                    break;
                }
            }
        }

        Transform regionArea = _areas[selectedArea].transform;

        Vector3 finalPos = Vector3.zero;
        int k = 100;
        for (int i = 0; i < k; i++)
        {
            Vector3 random = Random.insideUnitCircle;
            random = new Vector3(random.x, 0, random.y);
            random *= _areas[selectedArea].areaRadius;

            if (Physics.Raycast(regionArea.TransformPoint(random), -transform.up, out RaycastHit hit, 100f))
                if (NavMeshUtils.ValidPosition(hit.point))
                {
                    finalPos = regionArea.TransformPoint(random);
                    break;
                }
        }

        return finalPos;
    }
}
