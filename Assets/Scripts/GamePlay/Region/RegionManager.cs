using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using KB.Utils;

public class RegionManager : DontDestroyOnLoad
{
    private const string DataPath = "GameData";
    private GameData _gameData;
    public static float timeLimit;
    public static float currentGameTime;
    public static float currentWorldAngle;

    /// <summary>From 0 to 1 how bad is the angle of the world </summary>
    public static float worldAngleDanger;
    public static Transform world;
    public static Region[] allRegions;
    public static float inclinationFactor;
    #region Iniialization
    [RuntimeInitializeOnLoadMethod]
    private static void Init() => CreateSingleton(typeof(RegionManager), name: "[Region Manager]");

    #endregion

    protected override void Awake()
    {
        _gameData = Resources.Load<GameData>(DataPath);
        timeLimit = _gameData.maxGameTime;
        base.Awake();
    }
    private void OnEnable()
    {
        GameManager.OnStateChanged += OnGameStateChange;
        //GameManager.SetState(GameState.Gameplay); //TODO: REMOVER ISTO DAQUI!!!
    }

    private void OnDisable()
    {
        GameManager.OnStateChanged -= OnGameStateChange;
    }

    private void OnGameStateChange(GameState gameState)
    {
        ResetGameTime();
        if (gameState == GameState.Gameplay)
        {
            RegisterWorld();
        }

    }

    private void RegisterWorld()
    {
        world = GameObject.FindGameObjectWithTag("World").transform;
        allRegions = world.GetComponentsInChildren<Region>();
    }

    void Update()
    {
        if (GameManager.State != GameState.Gameplay || GameManager.IsPaused)
            return;
        IncreaseGameTime();
        CheckWorldInclination();
    }

    private void CheckWorldInclination()
    {
        float currentAngle = Vector3.Angle(world.transform.up, Vector3.up);
        currentWorldAngle = currentAngle;
        worldAngleDanger = Mathf.Clamp(currentWorldAngle / _gameData.maxWorldInclination, 0, 1);
        inclinationFactor = Mathf.InverseLerp(0f, _gameData.maxWorldInclination, currentAngle);
        if (currentAngle > _gameData.maxWorldInclination)
        {
            string gameTime = string.Format("{0}:{1:00}:{2:00}", (int)currentGameTime / 60, (int)currentGameTime % 60, ((decimal)currentGameTime % 1) * 100);
            GameManager.SetState(GameState.Result);
            StartCoroutine(OpenScoreRoutine(gameTime));
        }
    }
    IEnumerator OpenScoreRoutine(string gameTime)
    {
        yield return new WaitForSeconds(1f);
        UIMenuFlow.OpenScore?.Invoke(gameTime);

    }
    private void IncreaseGameTime()
    {
        currentGameTime += Time.deltaTime;
    }

    private void ResetGameTime()
    {
        currentGameTime = 0;
    }

    public static float GetGameTime()
    {
        return currentGameTime / timeLimit;
    }
}
