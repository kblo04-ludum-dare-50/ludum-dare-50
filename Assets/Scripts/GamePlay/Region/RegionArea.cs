using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegionArea : MonoBehaviour
{
    public float areaRadius;

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.matrix = transform.localToWorldMatrix;
        Gizmos.DrawWireSphere(Vector3.zero, areaRadius);
    }
}
