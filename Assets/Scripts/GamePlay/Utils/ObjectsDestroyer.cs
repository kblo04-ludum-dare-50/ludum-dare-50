using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectsDestroyer : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Body") || other.CompareTag("Building"))
        {
            other.GetComponent<DynamicObj>().DestroySelf();
        }
    }
}
