using UnityEngine;
using UnityEngine.AI;

public static class NavMeshUtils
{
    public static bool ValidPosition(Vector3 _pos)
    {
        if(NavMesh.SamplePosition(_pos, out NavMeshHit hit, .5f, NavMesh.AllAreas))
        {
            return true;
        }
        return false;
    }
}
